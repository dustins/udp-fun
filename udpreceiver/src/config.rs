use std::net::SocketAddr;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "udpreceiver")]
pub struct Config {
    /// Whether to display debug information
    #[structopt(short, long)]
    pub debug: bool,

    /// How many buffers to create
    #[structopt(short, long, default_value = "1024")]
    pub buffer_depth: usize,

    /// Size of packets to send
    #[structopt(short, long, default_value = "8192")]
    pub packet_size: usize,

    /// How many threads to send with
    #[structopt(short, long, default_value = "1")]
    pub threads: usize,

    /// Where to send the data
    #[structopt(name = "LISTEN")]
    pub listen: SocketAddr,
}