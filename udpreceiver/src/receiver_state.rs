use std::net::SocketAddr;
use std::sync::atomic::AtomicUsize;

use crate::config::Config;

pub struct ReceiverState {
    pub name: String,
    pub listen: SocketAddr,
    pub buffer_depth: usize,
    pub payload_size: usize,
    pub pps: AtomicUsize,
    pub bps: AtomicUsize,
}

impl ReceiverState {
    pub fn new(config: &Config, name: String) -> ReceiverState {
        ReceiverState {
            name,
            listen: config.listen,
            buffer_depth: config.buffer_depth,
            payload_size: config.packet_size,
            pps: AtomicUsize::new(0),
            bps: AtomicUsize::new(0),
        }
    }
}