#[derive(Debug)]
pub struct Stats {
    pub pps: usize,
    pub bps: usize,
}

impl Stats {
    pub fn new() -> Stats {
        Stats {
            pps: 0,
            bps: 0,
        }
    }
}