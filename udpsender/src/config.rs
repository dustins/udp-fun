use std::error::Error;
use std::net::SocketAddr;
use std::sync::atomic::AtomicIsize;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "udpsender")]
pub struct Config {
    /// Whether to display debug information
    #[structopt(short, long)]
    pub debug: bool,

    /// How many buffers to create
    #[structopt(short, long, default_value = "1024")]
    pub buffer_depth: usize,

    /// Size of packets to send
    #[structopt(short, long, default_value = "8192")]
    pub packet_size: usize,

    /// How many bytes to send
    #[structopt(short, long, parse(try_from_str = parse_size))]
    pub size: Option<AtomicIsize>,

    /// How many threads to send with
    #[structopt(short, long, default_value = "1")]
    pub threads: usize,

    /// Where to send the data
    #[structopt(name = "DESTINATION")]
    pub destination: SocketAddr,
}

fn parse_size(size: &str) -> Result<AtomicIsize, Box<dyn Error>> {
    let bytes = size.parse::<isize>().unwrap();
    Ok(AtomicIsize::new(bytes))
}