use std::net::SocketAddr;
use std::sync::Arc;
use std::sync::atomic::Ordering;
use std::time::Duration;

use crossbeam;
use crossbeam::channel::select;
use socket2::{Domain, Protocol, SockAddr, Socket, Type};
use structopt::StructOpt;

use crate::config::Config;
use crate::sender_state::SenderState;
use crate::stats::Stats;

mod stats;

mod sender_state;

mod config;

fn thread_loop(config: Arc<Config>, state: Arc<SenderState>, _sender_tx: crossbeam::Sender<()>) {
    let sock = Socket::new(Domain::ipv4(), Type::dgram(), Some(Protocol::udp())).unwrap();
    sock.set_reuse_port(true).expect("Unable to set SO_REUSEPORT for socket.");
    if config.debug {
        println!("[{}] Creating connection to {:?}", state.name, config.destination);
    }
    sock.bind(&"0.0.0.0:0".parse::<SocketAddr>().unwrap().into()).expect("Unable to bind to port.");
    sock.set_broadcast(true).expect("Unable to enable broadcast.");
    sock.connect(&SockAddr::from(config.destination)).expect("Unable to connect to destination.");

    let mut payload = Vec::with_capacity(config.packet_size);
    payload.resize(config.packet_size, 0u8);

    loop {
        let max_payload = match &config.size {
            Some(atomic) => {
                let current = atomic.fetch_sub(config.packet_size as isize, Ordering::SeqCst);
                if current <= 0 {
                    break;
                } else if current <= config.packet_size as isize {
                    current as usize
                } else {
                    config.packet_size
                }
            }
            None => config.packet_size
        };

        match sock.send(&payload[..max_payload]) {
            Ok(bytes_written) => {
                state.pps.fetch_add(1, Ordering::SeqCst);
                state.bps.fetch_add(bytes_written, Ordering::SeqCst);
            }
            Err(e) => panic!("[{}] Failed to send packet. {}", state.name, e)
        }
    }
}

fn main() {
    let config: Arc<Config> = Arc::new(Config::from_args());

    if config.debug {
        println!("config = {:?}", config);
    }

    let mut states = Vec::with_capacity(config.threads);
    let (sender_tx, sender_rx) = crossbeam::bounded(1);
    for i in 0..config.threads {
        let config = config.clone();
        let sender_tx = sender_tx.clone();
        let state = Arc::new(SenderState::new(&config, format!("udpsender-{}", i)));
        states.push(state.clone());
        std::thread::spawn(move || thread_loop(config, state, sender_tx));
    }
    drop(sender_tx);

    let ctrl_c_events = ctrl_channel().expect("Unable to get Ctrl-c channel");
    let ticks = crossbeam::tick(Duration::from_secs(1));

    let mut stats = Stats::new();
    loop {
        select! {
            recv(sender_rx) -> _ => {
                println!("Sending completed.");
                break;
            }
            recv(ticks) -> _ => {
                print_status(&mut states, &mut stats);
            }
            recv(ctrl_c_events) -> _ => {
                println!("Exiting program...");
                break;
            }
        }
    }
    print_total(&mut states);
}

fn ctrl_channel() -> Result<crossbeam::Receiver<()>, ctrlc::Error> {
    let (sender, receiver) = crossbeam::bounded(10);
    ctrlc::set_handler(move || {
        let _ = sender.send(());
    })?;

    Ok(receiver)
}

fn print_total(states: &mut Vec<Arc<SenderState>>) {
    let (pps_total, bps_total) = state_totals(states);

    println!("{:.3}M packets {:.3}MiB {:.3}Mb ({} packets, {} bytes)",
             pps_total as f64 / 1000.0 / 1000.0,
             bps_total as f64 / 1024.0 / 1024.0,
             bps_total as f64 * 8.0 / 1000.0 / 1000.0,
             pps_total,
             bps_total
    );
}

fn print_status(states: &mut Vec<Arc<SenderState>>, stats: &mut Stats) {
    let (pps_total, bps_total) = state_totals(states);

    // find the delta
    let pps = pps_total - stats.pps;
    let bps = bps_total - stats.bps;

    // update the stats
    stats.pps = pps_total;
    stats.bps = bps_total;

    println!("{:.3}M pps {:.3}MiB {:.3}Mb",
             pps as f64 / 1000.0 / 1000.0,
             bps as f64 / 1024.0 / 1024.0,
             bps as f64 * 8.0 / 1024.0 / 1024.0);
}

fn state_totals(states: &mut Vec<Arc<SenderState>>) -> (usize, usize) {
    let mut pps_total = 0;
    let mut bps_total = 0;
    for state in states {
        pps_total += state.pps.load(Ordering::SeqCst);
        bps_total += state.bps.load(Ordering::SeqCst);
    }
    (pps_total, bps_total)
}