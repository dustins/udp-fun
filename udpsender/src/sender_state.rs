use std::net::SocketAddr;
use std::sync::atomic::AtomicUsize;

use crate::config::Config;

pub struct SenderState {
    pub name: String,
    pub destination: SocketAddr,
    pub buffer_depth: usize,
    pub payload_size: usize,
    pub pps: AtomicUsize,
    pub bps: AtomicUsize,
}

impl SenderState {
    pub fn new(config: &Config, name: String) -> SenderState {
        SenderState {
            name,
            destination: config.destination,
            buffer_depth: config.buffer_depth,
            payload_size: config.packet_size,
            pps: AtomicUsize::new(0),
            bps: AtomicUsize::new(0),
        }
    }
}